# Generated by Django 4.1.7 on 2023-07-05 09:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ClaudeMonet', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ingredients',
            name='description',
        ),
    ]
