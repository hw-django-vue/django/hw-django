from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Chef(models.Model):
    name = models.CharField(max_length=100)
    specialty = models.CharField(max_length=100)
    work_experience = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(80)])

    def __str__(self):
        return self.name


class Nutrrinfo(models.Model):
    calories = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    fat = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    protein = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    carbohydrates = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)])


class Ingredients(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    nutrrinfo = models.OneToOneField(Nutrrinfo, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Dish(models.Model):
    name = models.CharField(max_length=100)
    chef = models.ForeignKey(Chef, on_delete=models.CASCADE)
    ingredients = models.ManyToManyField(Ingredients)

    def __str__(self):
        return self.name
