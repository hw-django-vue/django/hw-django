from rest_framework import serializers
from .models import Chef, Dish, Nutrrinfo, Ingredients
from django.db import models


class ChefSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chef
        fields = '__all__'


class NutrrinfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nutrrinfo
        fields = ('calories', 'fat', 'protein', 'carbohydrates')


class IngredientsSerializer(serializers.ModelSerializer):
    nutrrinfo = NutrrinfoSerializer()

    class Meta:
        model = Ingredients
        fields = '__all__'

    def update(self, instance, validated_data):
        nutrrinfo_data = validated_data.pop('nutrrinfo', None)
        if nutrrinfo_data:
            nutrrinfo_serializer = NutrrinfoSerializer(instance.nutrrinfo, data=nutrrinfo_data)
            nutrrinfo_serializer.is_valid(raise_exception=True)
            nutrrinfo_serializer.save()

        return super().update(instance, validated_data)

    def create(self, validated_data):
        nutrrinfo_data = validated_data.pop('nutrrinfo', None)
        if nutrrinfo_data:
            nutrrinfo_serializer = NutrrinfoSerializer(data=nutrrinfo_data)
            nutrrinfo_serializer.is_valid(raise_exception=True)
            nutrrinfo = nutrrinfo_serializer.save()
            validated_data['nutrrinfo'] = nutrrinfo

        return super().create(validated_data)


class DishSerializer(serializers.ModelSerializer):
    chef = serializers.PrimaryKeyRelatedField(queryset=Chef.objects.all())
    ingredients = serializers.PrimaryKeyRelatedField(queryset=Ingredients.objects.all(), many=True)

    class Meta:
        model = Dish
        fields = '__all__'

