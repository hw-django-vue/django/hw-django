from django.contrib import admin
from django.urls import path, include
from ClaudeMonet.views import *
from rest_framework.authtoken import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'ChefList', ChefViewSet)
router.register(r'DishesList', DishViewSet)
router.register(r'IngredList', IngredientsViewSet)

urlpatterns = [
    path('api/', include(router.urls))
]