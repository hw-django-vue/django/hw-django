from django.contrib import admin
from django.urls import path, include
from rest_framework import routers, urls

router = routers.DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('ClaudeMonet.urls')),
    path('', include('user.urls')),

]
